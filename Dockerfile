FROM adoptopenjdk:11-jdk AS build

RUN mkdir /opt/build
WORKDIR /opt/build

COPY . /opt/build/

RUN ./gradlew bootJar

FROM adoptopenjdk:11-jre AS deploy

RUN mkdir /opt/movierecommend
WORKDIR /opt/movierecommend

COPY --from=build /opt/build/build/libs/backend*.jar ./movierecommend.jar

CMD java -jar movierecommend.jar

EXPOSE 8080