# WebSocket API

WebSocket API for this application is implemented on top of the [STOMP](https://stomp.github.io/) protocol.

API is located at `http://<server address>/ws-api`.
## Authentication

No authentication is needed to connect to the API, however, a token must be populated when sending a subscription
request.
Effectively, the token used for WebSocket API is the same one used for the REST API.

Token is expected to be a value of the `x-auth-token` STOMP header.

## Channels

There is one channel populated for each room, available on the `/match/{roomId}` destination.
When every user in the room likes the same movie (i.e. the 'match' happens), a message is populated
to the channel dedicated to this room, containing a JSON object with only one parameter, `movieId`.

