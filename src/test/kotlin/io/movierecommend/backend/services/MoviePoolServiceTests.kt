package io.movierecommend.backend.services

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.MoviePoolEntry
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.exceptions.NoMoviesLeftException
import io.movierecommend.backend.repositories.MoviePoolRepository
import io.movierecommend.backend.services.recommend.MovieRecommendService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MoviePoolServiceTests {
	@Autowired
	private lateinit var poolService: MoviePoolService

	@MockBean
	private lateinit var poolRepository: MoviePoolRepository

	@MockBean
	private lateinit var appearanceService: UserMovieAppearanceService

	@MockBean
	private lateinit var recommendService: MovieRecommendService

	private val user = User("abap", "abap")

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	private val pool = listOf(
		MoviePoolEntry(
			room = room,
			movie = Movie(
				primaryName = "A Movie",
				enName = null,
				description = "This is a movie",
				poster = Movie.Poster(
					previewUrl = "http://preview",
					url = "http://fullimage"
				),
				rating = Movie.Rating(
					kinopoisk = 3.0,
					imdb = 3.0,
					tmdb = null
				),
				internalGenres = listOf(),
				persons = listOf(),
				names = listOf(Movie.Name("Фильм"))
			).also { it.id = ObjectId.get() }
		),
		MoviePoolEntry(
			room = room,
			movie = Movie(
				primaryName = "A Movie",
				enName = null,
				description = "This is a movie",
				poster = Movie.Poster(
					previewUrl = "http://preview",
					url = "http://fullimage"
				),
				rating = Movie.Rating(
					kinopoisk = 3.0,
					imdb = 3.0,
					tmdb = null
				),
				internalGenres = listOf(),
				persons = listOf(),
				names = listOf(Movie.Name("Фильм"))
			).also { it.id = ObjectId.get() }
		),
	)

	@BeforeEach
	fun setup() {
		Mockito
			.`when`(poolRepository.findAllByRoomId(room.id))
			.thenReturn(pool)
	}

	@Test
	fun getPoolContents() {
		val contents = poolService.getPoolContents(room)

		Mockito.verify(poolRepository).findAllByRoomId(room.id)

		Assertions.assertIterableEquals(pool, contents)
	}

	@Test
	fun pullMovieForUser_successWithRefillNotUsed() {
		Mockito
			.`when`(
				appearanceService.didUserSeeMovie(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn(false)

		poolService.pullMovieForUser(user, room, refill = true)

		Mockito
			.verify(recommendService, Mockito.never())
			.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject())
	}

	@Test
	fun pullMovieForUser_successWithRefillUsed() {
		Mockito
			.`when`(
				appearanceService.didUserSeeMovie(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn(true, *BooleanArray(pool.size - 1).map { true }.toTypedArray())
			.thenReturn(false)
		Mockito
			.`when`(recommendService.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject()))
			.thenReturn(pool.map { it.movie })

		poolService.pullMovieForUser(user, room, refill = true)

		Mockito
			.verify(recommendService)
			.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject())
		Mockito
			.verify(poolRepository, Mockito.times(pool.size))
			.save(MockitoKotlinHelper.anyObject())
	}

	@Test
	fun pullMovieForUser_successWithRefillByDefault() {
		Mockito
			.`when`(
				appearanceService.didUserSeeMovie(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn(true, *BooleanArray(pool.size - 1).map { true }.toTypedArray())
			.thenReturn(false)
		Mockito
			.`when`(recommendService.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject()))
			.thenReturn(pool.map { it.movie })

		poolService.pullMovieForUser(user, room)

		Mockito
			.verify(recommendService)
			.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject())
		Mockito
			.verify(poolRepository, Mockito.times(pool.size))
			.save(MockitoKotlinHelper.anyObject())
	}

	@Test
	fun pullMovieForUser_notEnoughWithRefillUsed() {
		Mockito
			.`when`(
				appearanceService.didUserSeeMovie(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn(true)

		Mockito
			.`when`(recommendService.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject()))
			.thenReturn(pool.map { it.movie })

		Assertions.assertThrows(NoMoviesLeftException::class.java) {
			poolService.pullMovieForUser(user, room, refill = true)
		}

		Mockito
			.verify(recommendService, Mockito.times(MoviePoolService.MAX_RETRIES))
			.next(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject())
		Mockito
			.verify(poolRepository, Mockito.times(pool.size * MoviePoolService.MAX_RETRIES))
			.save(MockitoKotlinHelper.anyObject())
	}

	@Test
	fun pullMovieForUser_successWithoutRefill() {
		Mockito.`when`(
			appearanceService.didUserSeeMovie(
				MockitoKotlinHelper.anyObject(),
				MockitoKotlinHelper.anyObject(),
				MockitoKotlinHelper.anyObject()
			)
		).thenReturn(false)

		poolService.pullMovieForUser(user, room, refill = false)

		Mockito
			.verify(recommendService, Mockito.never())
			.next(ArgumentMatchers.anyList(), ArgumentMatchers.anyList())
	}

	@Test
	fun pullMovieForUser_notEnoughWithoutRefill() {
		Mockito.`when`(
			appearanceService.didUserSeeMovie(
				MockitoKotlinHelper.anyObject(),
				MockitoKotlinHelper.anyObject(),
				MockitoKotlinHelper.anyObject()
			)
		).thenReturn(true)

		Assertions.assertThrows(NoMoviesLeftException::class.java) {
			poolService.pullMovieForUser(user, room, refill = false)
		}
	}
}
