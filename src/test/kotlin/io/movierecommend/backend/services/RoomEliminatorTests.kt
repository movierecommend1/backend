package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.repositories.MoviePoolRepository
import io.movierecommend.backend.repositories.RoomRepository
import io.movierecommend.backend.repositories.UserMovieAppearanceRepository
import org.bson.types.ObjectId
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RoomEliminatorTests {
	@MockBean
	private lateinit var roomRepository: RoomRepository

	@MockBean
	private lateinit var poolRepository: MoviePoolRepository

	@MockBean
	private lateinit var appearanceRepository: UserMovieAppearanceRepository

	@Autowired
	private lateinit var roomEliminator: RoomEliminator

	private val user = User("abap", "abap")

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	@Test
	fun eliminateRoom_deletesPools() {
		roomEliminator.eliminate(room)

		Mockito.verify(poolRepository).deleteAllByRoomId(room.id)
	}

	@Test
	fun eliminateRoom_deletesAppearances() {
		roomEliminator.eliminate(room)

		Mockito.verify(appearanceRepository).deleteAllByRoomId(room.id)
	}

	@Test
	fun eliminateRoom_deletesRoom() {
		roomEliminator.eliminate(room)

		Mockito.verify(roomRepository).delete(room)
	}
}
