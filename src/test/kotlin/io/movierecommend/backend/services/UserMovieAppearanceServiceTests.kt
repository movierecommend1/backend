package io.movierecommend.backend.services

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserMovieAppearance
import io.movierecommend.backend.repositories.UserMovieAppearanceRepository
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserMovieAppearanceServiceTests {
	@Autowired
	private lateinit var appearanceService: UserMovieAppearanceService

	@MockBean
	private lateinit var appearanceRepository: UserMovieAppearanceRepository

	private val user = User("abap", "abap")
		.also { it.id = ObjectId.get() }
	private val anotherUser = User("foo", "bar")
		.also { it.id = ObjectId.get() }

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user, anotherUser),
	).also { it.id = ObjectId.get() }

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	private val anotherMovie = movie.copy().also { it.id = ObjectId.get() }

	private val appearances = listOf(
		UserMovieAppearance(
			user, movie, room, liked = false
		),
		UserMovieAppearance(
			user = anotherUser,
			movie, room, liked = false
		),
		UserMovieAppearance(
			user,
			movie = anotherMovie,
			room,
			liked = true
		),
		UserMovieAppearance(
			user = anotherUser,
			movie = anotherMovie,
			room,
			liked = true
		)
	).map { ap -> ap.also { it.id = ObjectId.get() } }

	@BeforeEach
	fun setup() {
		Mockito
			.`when`(appearanceRepository.findAllByRoomId(MockitoKotlinHelper.anyObject()))
			.then { invocation ->
				appearances.filter { it.room.id == invocation.arguments.first() }
			}
		Mockito
			.`when`(
				appearanceRepository.find(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.then { invocation ->
				appearances
					.filter { it.user.id == invocation.arguments[0] }
					.filter { it.movie.id == invocation.arguments[1] }
					.firstOrNull { it.room.id == invocation.arguments[2] }
			}
		Mockito
			.`when`(
				appearanceRepository.findAllByRoomAndMovieId(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.then { invocation ->
				appearances
					.filter { it.room.id == invocation.arguments[0] }
					.filter { it.movie.id == invocation.arguments[1] }
			}
		Mockito
			.`when`(appearanceRepository.save(MockitoKotlinHelper.anyObject()))
			.then { it.arguments.first() }
	}

	@Test
	fun getAllForRoom() {
		val found = appearanceService.getAllForRoom(room)

		Assertions.assertIterableEquals(appearances, found)
	}

	@Test
	fun didUserSeeMovie_seen() {
		Assertions.assertTrue { appearanceService.didUserSeeMovie(room, user, movie) }
	}

	@Test
	fun didUserSeeMovie_didntSee() {
		val unkMovie = movie.copy().also { it.id = ObjectId.get() }

		Assertions.assertFalse { appearanceService.didUserSeeMovie(room, user, unkMovie) }
	}

	@Test
	fun markMovieSeen_newMovieNotLiked() {
		val unkMovie = movie.copy().also { it.id = ObjectId.get() }

		val appearance = appearanceService.markMovieSeen(room, user, unkMovie, liked = false)

		Mockito.verify(appearanceRepository).save(appearance)

		Assertions.assertFalse { appearance.liked }
	}

	@Test
	fun markMovieSeen_newMovieByDefault() {
		val unkMovie = movie.copy().also { it.id = ObjectId.get() }

		val appearance = appearanceService.markMovieSeen(room, user, unkMovie)

		Mockito.verify(appearanceRepository).save(appearance)

		Assertions.assertFalse { appearance.liked }
	}

	@Test
	fun markMovieSeen_likeExistingMovie() {
		val movie = movie

		val appearance = appearanceService.markMovieSeen(room, user, movie, liked = true)

		Mockito.verify(appearanceRepository).save(appearance)

		Assertions.assertTrue { appearance.liked }
		Assertions.assertEquals(movie.id, appearance.movie.id)
	}

	@Test
	fun isMovieAMatch_itIs() {
		Assertions.assertTrue { appearanceService.isMovieAMatch(room, anotherMovie) }
	}

	@Test
	fun isMovieAMatch_itIsnt() {
		Assertions.assertFalse { appearanceService.isMovieAMatch(room, movie) }
	}
}
