package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.messages.MatchNotification
import io.movierecommend.backend.messages.RoomStartNotification
import org.bson.types.ObjectId
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessagingServiceTests {
	@Autowired
	private lateinit var messagingService: MessagingService

	@MockBean
	private lateinit var destinationProcessor: DestinationProcessor

	@MockBean
	private lateinit var messagingTemplate: SimpMessagingTemplate

	private val user = User("abap", "abap")

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	@Test
	fun sendMatchNotification_sendsMatch() {
		Mockito
			.`when`(
				destinationProcessor.composeDestination(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn("/match/ABA")

		messagingService.sendMatchNotification(movie, room)

		val message = MatchNotification(
			movieId = movie.id.toHexString()
		)

		Mockito
			.verify(messagingTemplate)
			.convertAndSend("/match/ABA", message)
	}

	@Test
	fun sendMatchNotification_sendsStart() {
		Mockito
			.`when`(
				destinationProcessor.composeDestination(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn("/start/ABA")

		messagingService.sendStartNotification(room)

		val message = RoomStartNotification(
			roomSlug = room.slug
		)

		Mockito
			.verify(messagingTemplate)
			.convertAndSend("/start/ABA", message)
	}
}
