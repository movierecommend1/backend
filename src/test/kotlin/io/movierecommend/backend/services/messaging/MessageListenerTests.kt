package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.events.MovieMatchedEvent
import io.movierecommend.backend.events.RoomStartedEvent
import org.bson.types.ObjectId
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MessageListenerTests {
	@Autowired
	private lateinit var messageListener: MessageListener

	@MockBean
	private lateinit var messagingService: MessagingService

	private val user = User("abap", "abap")

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	@Test
	fun onMovieMatched_sendsMessage() {
		val event = MovieMatchedEvent(
			emitter = this,
			room, movie
		)

		messageListener.onMovieMatched(event)

		Mockito.verify(messagingService)
			.sendMatchNotification(movie, room)
	}

	@Test
	fun onRoomStarted_sendsMessage() {
		val event = RoomStartedEvent(
			emitter = this,
			room
		)

		messageListener.onRoomStarted(event)

		Mockito.verify(messagingService)
			.sendStartNotification(room)
	}
}
