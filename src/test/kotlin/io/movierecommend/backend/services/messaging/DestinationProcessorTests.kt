package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DestinationProcessorTests {
	@Autowired
	private lateinit var destinationProcessor: DestinationProcessor

	private val user = User("abap", "abap")

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	@Test
	fun composeDestination_producesDestinationMatch() {
		val dest = destinationProcessor.composeDestination(room, DestinationProcessor.DestinationType.MATCH)

		Assertions.assertEquals("/match/${room.id.toHexString()}", dest)
	}
	@Test
	fun composeDestination_producesDestinationStart() {
		val dest = destinationProcessor.composeDestination(room, DestinationProcessor.DestinationType.START)

		Assertions.assertEquals("/start/${room.id.toHexString()}", dest)
	}

	@Test
	fun parseDestination_wrongPathLessParts() {
		val dest = destinationProcessor.parseDestination("/room")

		Assertions.assertNull(dest)
	}

	@Test
	fun parseDestination_wrongPathMoreParts() {
		val dest = destinationProcessor.parseDestination("/I/hate/writing/tests")

		Assertions.assertNull(dest)
	}

	@Test
	fun parseDestination_wrongPathNotAbsolute() {
		val dest = destinationProcessor.parseDestination("plunk/room/ABA")

		Assertions.assertNull(dest)
	}

	@Test
	fun parseDestination_wrongPathInvalidType() {
		val dest = destinationProcessor.parseDestination("/room/ABA")

		Assertions.assertNull(dest)
	}

	@Test
	fun parseDestination_parsedMatch() {
		val roomSlug = "ABA"

		val parsed = destinationProcessor.parseDestination("/match/$roomSlug")

		Assertions.assertNotNull(parsed)
		Assertions.assertEquals(roomSlug, parsed!!.roomSlug)
		Assertions.assertEquals(DestinationProcessor.DestinationType.MATCH, parsed.type)
	}
	@Test
	fun parseDestination_parsedStart() {
		val roomSlug = "ABA"

		val parsed = destinationProcessor.parseDestination("/start/$roomSlug")

		Assertions.assertNotNull(parsed)
		Assertions.assertEquals(roomSlug, parsed!!.roomSlug)
		Assertions.assertEquals(DestinationProcessor.DestinationType.START, parsed.type)
	}
}
