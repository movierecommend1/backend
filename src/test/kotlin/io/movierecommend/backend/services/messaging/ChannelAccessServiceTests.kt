package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserPrincipal
import io.movierecommend.backend.exceptions.InvalidChannelException
import io.movierecommend.backend.exceptions.NoRoomException
import io.movierecommend.backend.services.RoomService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChannelAccessServiceTests {
	@Autowired
	private lateinit var accessService: ChannelAccessService

	@MockBean
	private lateinit var roomService: RoomService

	@MockBean
	private lateinit var destinationProcessor: DestinationProcessor

	private val user = User("abap", "abap")
		.also { it.id = ObjectId.get() }

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	private val principal = UserPrincipal(
		id = user.id,
		username = user.username
	)

	@BeforeEach
	fun setup() {
		Mockito
			.`when`(destinationProcessor.parseDestination(anyString()))
			.thenReturn(
				DestinationProcessor.ParsedDestination(
					roomSlug = room.slug,
					type = DestinationProcessor.DestinationType.MATCH
				)
			)

		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(room)
	}

	@Test
	fun checkSubscribeDestinationAccess_invalidDestination() {
		Mockito
			.`when`(destinationProcessor.parseDestination(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(InvalidChannelException::class.java) {
			accessService.checkSubscribeDestinationAccess(principal, "I/hate/unit/testing")
		}
	}

	@Test
	fun checkSubscribeDestinationAccess_invalidRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			accessService.checkSubscribeDestinationAccess(principal, "I/hate/unit/testing/so/much")
		}
	}

	@Test
	fun checkSubscribeDestinationAccess_notInRoom() {
		val principal = UserPrincipal(
			id = ObjectId.get(),
			username = "foobar"
		)

		Assertions.assertFalse {
			accessService.checkSubscribeDestinationAccess(principal, "I/cant/do/anymore")
		}
	}

	@Test
	fun checkSubscribeDestinationAccess_granted() {
		Assertions.assertTrue {
			accessService.checkSubscribeDestinationAccess(principal, "I/cant/do/anymore")
		}
	}
}
