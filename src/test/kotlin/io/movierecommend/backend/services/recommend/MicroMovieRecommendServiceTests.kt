package io.movierecommend.backend.services.recommend

import com.fasterxml.jackson.databind.ObjectMapper
import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.config.properties.RecommendProperties
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.services.MovieService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.client.RestTemplate

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MicroMovieRecommendServiceTests {
	private lateinit var recommendService: MicroMovieRecommendService

	private lateinit var restTemplateBuilder: RestTemplateBuilder

	@MockBean
	private lateinit var movieService: MovieService

	private lateinit var restTemplate: RestTemplate

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	private val anotherMovie = movie.copy().also { it.id = ObjectId.get() }

	@BeforeEach
	fun setup(
		@Autowired properties: RecommendProperties,
		@Autowired objectMapper: ObjectMapper
	) {
		restTemplateBuilder = Mockito.mock(RestTemplateBuilder::class.java)
		restTemplate = Mockito.mock(RestTemplate::class.java)

		Mockito.`when`(restTemplateBuilder.rootUri(anyString()))
			.thenReturn(restTemplateBuilder)
		Mockito.`when`(restTemplateBuilder.build())
			.thenReturn(restTemplate)

		recommendService = MicroMovieRecommendService(
			properties, restTemplateBuilder, objectMapper,
			movieService
		)
	}

	@Test
	fun next_recommends(@Autowired objectMapper: ObjectMapper) {
		val response = Mockito.mock(ResponseEntity::class.java)

		Mockito
			.`when`(response.body)
			.thenReturn(listOf<String>(movie.id.toHexString()))

		val liked = listOf(movie)
		val seen = listOf(movie, anotherMovie)

		Mockito
			.`when`(
				restTemplate.exchange(
					anyString(),
					ArgumentMatchers.eq(HttpMethod.GET),
					ArgumentMatchers.eq(HttpEntity.EMPTY),
					MockitoKotlinHelper.anyObject<ParameterizedTypeReference<*>>(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn(response)

		Assertions.assertDoesNotThrow {
			recommendService.next(liked, seen)
		}

		Mockito
			.verify(movieService)
			.getMovieById(movie.id.toHexString())
	}

	@Test
	fun next_bodyNull(@Autowired objectMapper: ObjectMapper) {
		val response = Mockito.mock(ResponseEntity::class.java)

		Mockito
			.`when`(response.body)
			.thenReturn(null)

		val liked = listOf(movie)
		val seen = listOf(movie, anotherMovie)

		Mockito
			.`when`(
				restTemplate.exchange(
					anyString(),
					ArgumentMatchers.eq(HttpMethod.GET),
					ArgumentMatchers.eq(HttpEntity.EMPTY),
					MockitoKotlinHelper.anyObject<ParameterizedTypeReference<*>>(),
					MockitoKotlinHelper.anyObject()
				)
			)
			.thenReturn(response)

		Assertions.assertThrows(IllegalStateException::class.java) {
			recommendService.next(liked, seen)
		}
	}
}
