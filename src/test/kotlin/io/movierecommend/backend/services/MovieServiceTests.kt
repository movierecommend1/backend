package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.repositories.MovieRepository
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MovieServiceTests {
	@MockBean
	private lateinit var movieRepository: MovieRepository

	@Autowired
	private lateinit var movieService: MovieService

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	@BeforeEach
	fun setup() {
		val movies = listOf(movie)

		Mockito
			.`when`(movieRepository.findById(any(ObjectId::class.java)))
			.then { invocation ->
				movies
					.firstOrNull { it.id == invocation.arguments.first() }
					.let { Optional.ofNullable(it) }
			}
	}

	@Test
	fun getMovieById_exists() {
		val found = movieService.getMovieById(movie.id.toHexString())

		Assertions.assertEquals(movie, found)
	}

	@Test
	fun getMovieById_dontExist() {
		val falsyId = ObjectId.get().toHexString()
		val found = movieService.getMovieById(falsyId)

		Assertions.assertNull(found)
	}
}
