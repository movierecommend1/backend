package io.movierecommend.backend.services.auth

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.movierecommend.backend.config.properties.SecurityProperties
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserPrincipal
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JWTAuthServiceTests {
	@Autowired
	private lateinit var jwtAuthService: JWTAuthService

	@Autowired
	private lateinit var securityProperties: SecurityProperties

	val user = User(
		username = "user",
		password = "password",
	).also { it.id = ObjectId.get() }

	@Test
	fun generateToken_setsValidClaims() {
		val token = jwtAuthService.generateToken(user)

		Assertions.assertEquals(user.id.toHexString(), parseTokenClaims(token).body["id"])
		Assertions.assertEquals(user.username, parseTokenClaims(token).body.subject)
	}

	@Test
	fun parseToken_getsValidClaims() {
		val parsed = jwtAuthService.parseToken(buildToken())

		Assertions.assertEquals(user.id, parsed.id)
		Assertions.assertEquals(user.username, parsed.username)
	}

	@Test
	fun createSecurityPrincipal_fromRawToken() {
		val principal = jwtAuthService.createSecurityPrincipal(buildToken())

		Assertions.assertEquals(user.username, principal.name)
	}

	@Test
	fun createSecurityPrincipal_fromUserPrincipal() {
		val principal = jwtAuthService.createSecurityPrincipal(
			UserPrincipal(
				id = user.id,
				username = user.username
			)
		)

		Assertions.assertEquals(user.username, principal.name)
	}

	private fun buildToken() =
		Jwts.builder()
			.claim("id", user.id.toHexString())
			.setSubject(user.username)
			.signWith(securityProperties.signingKey, SignatureAlgorithm.HS384)
			.compact()

	private fun parseTokenClaims(token: String) =
		Jwts.parserBuilder()
			.setSigningKey(securityProperties.signingKey)
			.build()
			.parseClaimsJws(token)
}
