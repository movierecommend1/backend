package io.movierecommend.backend.services.auth

import io.movierecommend.backend.documents.User
import io.movierecommend.backend.services.UserService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MongoUserDetailsServiceTests {
	@Autowired
	private lateinit var userDetailsService: MongoUserDetailsService

	@MockBean
	private lateinit var userService: UserService

	@Test
	fun loadByUsername_emptyUsername() {
		Assertions.assertThrows(UsernameNotFoundException::class.java) {
			userDetailsService.loadUserByUsername(null)
		}
	}

	@Test
	fun loadByUsername_missingUsername() {
		Mockito
			.`when`(userService.findUser(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(UsernameNotFoundException::class.java) {
			userDetailsService.loadUserByUsername("foobar")
		}
	}

	@Test
	fun loadByUsername_findsUser() {
		val user = User(username = "foobar", password = "1234")
		Mockito
			.`when`(userService.findUser(user.username))
			.thenReturn(user)

		Assertions.assertEquals(user, userDetailsService.loadUserByUsername(user.username))
	}
}
