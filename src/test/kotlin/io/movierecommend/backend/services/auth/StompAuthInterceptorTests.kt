package io.movierecommend.backend.services.auth

import io.jsonwebtoken.MalformedJwtException
import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.UserPrincipal
import io.movierecommend.backend.services.UserService
import io.movierecommend.backend.services.messaging.ChannelAccessService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.* //
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.test.context.junit.jupiter.SpringExtension
import javax.naming.AuthenticationException

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StompAuthInterceptorTests {
	@Autowired
	private lateinit var stompAuthInterceptor: StompAuthInterceptor

	@MockBean
	private lateinit var authService: AuthService

	@MockBean
	private lateinit var userService: UserService

	@MockBean
	private lateinit var channelAccessService: ChannelAccessService

	private val mockChannel = Mockito.mock(MessageChannel::class.java)

	private val mockMessage = Mockito.mock(Message::class.java)
	private val mockAccessor = Mockito.mock(StompHeaderAccessor::class.java)
	private val accessorStatic = Mockito
		.mockStatic(MessageHeaderAccessor::class.java)

	@BeforeEach
	fun setup() {
		accessorStatic
			.`when`<MessageHeaderAccessor> {
				MessageHeaderAccessor.getAccessor<StompHeaderAccessor>(
					MockitoKotlinHelper.anyObject<Message<*>>(),
					MockitoKotlinHelper.anyObject()
				)
			}
			.thenReturn(mockAccessor)

		Mockito
			.`when`(mockAccessor.command)
			.thenReturn(StompCommand.SUBSCRIBE)

		Mockito
			.`when`(authService.parseToken(anyString()))
			.thenReturn(
				UserPrincipal(id = ObjectId.get(), username = "foobar")
			)

		Mockito
			.`when`(mockAccessor.destination)
			.thenReturn("/room")

		Mockito
			.`when`(mockAccessor.getFirstNativeHeader(StompAuthInterceptor.TOKEN_HEADER))
			.thenReturn("tokn")

		Mockito
			.`when`(
				channelAccessService.checkSubscribeDestinationAccess(
					MockitoKotlinHelper.anyObject(),
					anyString()
				)
			)
			.thenReturn(true)

		Mockito
			.`when`(userService.usernameExists(anyString()))
			.thenReturn(true)
	}

	@Test
	fun preSend_noAccessor() {
		accessorStatic
			.`when`<MessageHeaderAccessor> {
				MessageHeaderAccessor.getAccessor<StompHeaderAccessor>(
					MockitoKotlinHelper.anyObject<Message<*>>(),
					MockitoKotlinHelper.anyObject()
				)
			}
			.thenReturn(null)

		val msg = mockMessage

		val filtered = stompAuthInterceptor.preSend(msg, mockChannel)

		Assertions.assertEquals(msg, filtered)
	}

	@Test
	fun preSend_notASubscribe() {
		Mockito.`when`(mockAccessor.command)
			.thenReturn(StompCommand.CONNECT)

		val msg = mockMessage

		val filtered = stompAuthInterceptor.preSend(msg, mockChannel)

		Assertions.assertEquals(msg, filtered)
	}

	@Test
	fun preSend_invalidToken() {
		Mockito
			.`when`(mockAccessor.getFirstNativeHeader(anyString()))
			.thenReturn("")
		Mockito
			.`when`(authService.parseToken(anyString()))
			.then { throw MalformedJwtException("") }

		Assertions.assertThrows(AuthenticationException::class.java) {
			stompAuthInterceptor.preSend(mockMessage, mockChannel)
		}
	}

	@Test
	fun preSend_invalidUsername() {
		Mockito
			.`when`(userService.usernameExists(anyString()))
			.thenReturn(false)

		Assertions.assertThrows(AuthenticationException::class.java) {
			stompAuthInterceptor.preSend(mockMessage, mockChannel)
		}
	}

	@Test
	fun preSend_channelEmptyDestination() {
		Mockito
			.`when`(mockAccessor.destination)
			.thenReturn(null)

		Mockito
			.`when`(
				channelAccessService.checkSubscribeDestinationAccess(
					MockitoKotlinHelper.anyObject(),
					anyString()
				)
			)
			.thenReturn(false)

		Assertions.assertThrows(AuthenticationException::class.java) {
			stompAuthInterceptor.preSend(mockMessage, mockChannel)
		}
	}

	@Test
	fun preSend_channelAccessDenied() {
		Mockito
			.`when`(
				channelAccessService.checkSubscribeDestinationAccess(
					MockitoKotlinHelper.anyObject(),
					anyString()
				)
			)
			.thenReturn(false)

		Assertions.assertThrows(AuthenticationException::class.java) {
			stompAuthInterceptor.preSend(mockMessage, mockChannel)
		}
	}

	@Test
	fun preSend_userAuthorized() {
		val result = stompAuthInterceptor.preSend(mockMessage, mockChannel)

		Mockito
			.verify(mockAccessor)
			.user = MockitoKotlinHelper.anyObject()

		Assertions.assertInstanceOf(Message::class.java, result)
	}
}
