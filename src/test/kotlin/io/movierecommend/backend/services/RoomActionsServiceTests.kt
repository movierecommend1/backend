package io.movierecommend.backend.services

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserMovieAppearance
import io.movierecommend.backend.events.MovieMatchedEvent
import io.movierecommend.backend.exceptions.NoMoviesLeftException
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RoomActionsServiceTests {
	private lateinit var actionsService: RoomActionsService

	@MockBean
	private lateinit var poolService: MoviePoolService

	@MockBean
	private lateinit var appearanceService: UserMovieAppearanceService

	@MockBean
	private lateinit var eventPublisher: ApplicationEventPublisher

	private val user = User("abap", "abap")
		.also { it.id = ObjectId.get() }

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	@BeforeEach
	fun setup() {
		actionsService = RoomActionsService(
			poolService, appearanceService, eventPublisher
		)

		Mockito
			.`when`(
				poolService.pullMovieForUser(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					refill = anyBoolean(),
				)
			)
			.thenReturn(movie)
	}

	@Test
	fun recommend_success() {
		val recMovie = actionsService.recommend(user, room)

		Mockito
			.verify(appearanceService)
			.markMovieSeen(room, user, recMovie, liked = false)

		Assertions.assertEquals(movie, recMovie)
	}

	@Test
	fun recommend_poolEmpty() {
		Mockito
			.`when`(
				poolService.pullMovieForUser(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
					refill = anyBoolean(),
				)
			)
			.then { throw NoMoviesLeftException() }

		Assertions.assertThrows(NoMoviesLeftException::class.java) {
			actionsService.recommend(user, room)
		}
	}

	@Test
	fun likeMovie_notYetMatch() {
		Mockito
			.`when`(
				appearanceService.isMovieAMatch(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
				)
			)
			.thenReturn(false)

		actionsService.likeMovie(user, room, movie)

		Mockito
			.verify(appearanceService)
			.markMovieSeen(room, user, movie, liked = true)
	}

	@Test
	fun likeMovie_matched() {
		Mockito
			.`when`(
				appearanceService.isMovieAMatch(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject(),
				)
			)
			.thenReturn(true)

		actionsService.likeMovie(user, room, movie)

		Mockito.verify(eventPublisher)
			.publishEvent(MockitoKotlinHelper.anyObject<MovieMatchedEvent>())
	}

	@Test
	fun getRoomStats_addsLikedToRank() {
		Mockito
			.`when`(
				appearanceService.getAllForRoom(MockitoKotlinHelper.anyObject())
			)
			.thenReturn(
				listOf(
					UserMovieAppearance(user, movie, room, liked = true)
				)
			)

		val stats = actionsService.getRoomStats(room)

		Assertions.assertTrue { stats.ranking.any { it.likedUsers.contains(user.id.toHexString()) } }
	}

	@Test
	fun getRoomStats_dontAddExcessToRank() {
		Mockito
			.`when`(
				appearanceService.getAllForRoom(MockitoKotlinHelper.anyObject())
			)
			.thenReturn(
				listOf(
					UserMovieAppearance(user, movie, room, liked = false)
				)
			)

		val stats = actionsService.getRoomStats(room)

		Assertions.assertFalse { stats.ranking.any { it.likedUsers.contains(user.id.toHexString()) } }
	}

	@Test
	fun getRoomStats_detectsMatches() {
		Mockito
			.`when`(
				appearanceService.getAllForRoom(MockitoKotlinHelper.anyObject())
			)
			.thenReturn(
				listOf(
					UserMovieAppearance(user, movie, room, liked = true)
				)
			)
		Mockito
			.`when`(
				appearanceService.isMovieAMatch(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)

			)
			.thenReturn(true)

		val stats = actionsService.getRoomStats(room)

		Assertions.assertTrue { stats.matchedMovies.contains(movie.id.toHexString()) }
	}

	@Test
	fun getRoomStats_dontGiveExcessMatches() {
		Mockito
			.`when`(
				appearanceService.getAllForRoom(MockitoKotlinHelper.anyObject())
			)
			.thenReturn(
				listOf(
					UserMovieAppearance(user, movie, room, liked = true)
				)
			)
		Mockito
			.`when`(
				appearanceService.isMovieAMatch(
					MockitoKotlinHelper.anyObject(),
					MockitoKotlinHelper.anyObject()
				)

			)
			.thenReturn(false)

		val stats = actionsService.getRoomStats(room)

		Assertions.assertFalse { stats.matchedMovies.contains(movie.id.toHexString()) }
	}
}
