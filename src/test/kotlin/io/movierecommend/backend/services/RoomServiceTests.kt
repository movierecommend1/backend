package io.movierecommend.backend.services

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.config.properties.RoomsProperties
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.events.RoomStartedEvent
import io.movierecommend.backend.exceptions.AlreadyInRoomException
import io.movierecommend.backend.exceptions.InvalidRoomStatusException
import io.movierecommend.backend.exceptions.RoomActionDeniedException
import io.movierecommend.backend.repositories.RoomRepository
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RoomServiceTests {

	@MockBean
	private lateinit var roomRepository: RoomRepository

	@MockBean
	private lateinit var slugGenerator: SlugGenerator

	@MockBean
	private lateinit var roomEliminator: RoomEliminator

	private lateinit var roomService: RoomService

	@Bean
	fun roomsProperties() = RoomsProperties(
		slugLength = 3
	)

	private val eventPublisher = Mockito.mock(ApplicationEventPublisher::class.java)

	private val users = listOf(
		User(
			username = "foo",
			password = "bar"
		).also { it.id = ObjectId.get() },
		User(
			username = "baz",
			password = "wok",
		).also { it.id = ObjectId.get() },
		User(
			username = "dum",
			password = "123",
		).also { it.id = ObjectId.get() }
	)

	private val rooms = listOf(
		Room(
			slug = "AAA",
			creator = users[0],
			users = users.subList(0, 2).toMutableList(),
		).also { it.id = ObjectId.get() },
		Room(
			slug = "ABA",
			creator = users[0],
			users = users.subList(0, 2).toMutableList(),
			status = Room.Status.Abandoned
		).also { it.id = ObjectId.get() },
		Room(
			slug = "BBB",
			creator = users[0],
			users = users.subList(0, 2).toMutableList(),
			status = Room.Status.Started
		).also { it.id = ObjectId.get() },

	)

	@BeforeEach
	fun setup(@Autowired roomsProperties: RoomsProperties) {
		roomService = RoomService(
			roomRepository, roomsProperties,
			eliminator = roomEliminator, slugGenerator, eventPublisher
		)

		Mockito
			.`when`(roomRepository.findAllByUserId(MockitoKotlinHelper.anyObject()))
			.then { invocation ->
				rooms.filter { room -> room.users.any { it.id == invocation.arguments.first() } }
			}
		Mockito
			.`when`(roomRepository.findBySlug(anyString()))
			.then { invocation ->
				rooms.firstOrNull { it.slug == invocation.arguments.first() }
			}
		Mockito
			.`when`(roomRepository.existsBySlug(anyString()))
			.then { invocation ->
				rooms.any { it.slug == invocation.arguments.first() }
			}
		Mockito
			.`when`(roomRepository.save(any()))
			.then { invocation -> invocation.arguments.first() }
		Mockito
			.`when`(slugGenerator.generate(anyInt()))
			.then { SlugGenerator().generate(it.arguments.first() as Int) }
		Mockito
			.`when`(roomEliminator.eliminate(MockitoKotlinHelper.anyObject()))
			.then { }
	}

	@Test
	fun getRoomsByUser_exists() {
		val found = roomService.getRoomsByUser(users[0])

		Assertions.assertTrue { found.contains(rooms[0]) }
	}

	@Test
	fun getRoomsByUser_existsNotCreator() {
		val found = roomService.getRoomsByUser(users[1])

		Assertions.assertTrue { found.contains(rooms[0]) }
	}

	@Test
	fun getRoomsByUser_dontExist() {
		val found = roomService.getRoomsByUser(users[2])

		Assertions.assertTrue { found.isEmpty() }
	}

	@Test
	fun getRoomBySlug_exists() {
		val found = roomService.getRoomBySlug("AAA")

		Assertions.assertNotNull(found)
	}

	@Test
	fun getRoomBySlug_notExist() {
		val found = roomService.getRoomBySlug("AAA")

		Assertions.assertNotNull(found)
	}

	@Test
	fun createRoom_generatesValidSlug(@Autowired roomsProperties: RoomsProperties) {
		val created = roomService.createRoom(users[2])

		Assertions.assertEquals(roomsProperties.slugLength, created.slug.length)
	}

	@Test
	fun createRoom_generatesValidSlugByCollision(@Autowired roomsProperties: RoomsProperties) {
		Mockito
			.`when`(slugGenerator.generate(anyInt()))
			.then { "AAA" }
			.then { SlugGenerator().generate(it.arguments.first() as Int) }

		val created = roomService.createRoom(users[2])

		Mockito.verify(slugGenerator, Mockito.atLeast(2)).generate(anyInt())

		Assertions.assertTrue { rooms.none { it.slug == created.slug } }
	}

	@Test
	fun createRoom_setsCorrectUsers() {
		val created = roomService.createRoom(users[2])

		Assertions.assertEquals(users[2], created.creator)
		Assertions.assertTrue { created.users.contains(users[2]) }
	}

	@Test
	fun createRoom_setsCorrectStatus() {
		val created = roomService.createRoom(users[2])

		Assertions.assertEquals(Room.Status.Created, created.status)
	}

	@Test
	fun joinRoom_joinsProperly() {
		val created = roomService.joinRoom(users[2], rooms[0])

		Assertions.assertTrue { created.users.contains(users[2]) }
	}

	@Test
	fun joinRoom_rejectsIfNotStatusCreated() {
		Assertions.assertThrows(InvalidRoomStatusException::class.java) {
			roomService.joinRoom(users[2], rooms[1])
		}
	}

	@Test
	fun joinRoom_rejectsIfUserInRoom() {
		Assertions.assertThrows(AlreadyInRoomException::class.java) {
			roomService.joinRoom(users[1], rooms[0])
		}
	}

	@Test
	fun startRoom_correctly() {
		val room = roomService.startRoom(rooms[0].creator, rooms[0])

		Mockito.verify(eventPublisher)
			.publishEvent(MockitoKotlinHelper.anyObject<RoomStartedEvent>())

		Assertions.assertEquals(Room.Status.Started, room.status)
	}

	@Test
	fun startRoom_statusNotCreated() {
		Assertions.assertThrows(InvalidRoomStatusException::class.java) {
			roomService.startRoom(rooms[1].creator, rooms[1])
		}
	}

	@Test
	fun startRoom_userNotPermitted() {
		Assertions.assertThrows(RoomActionDeniedException::class.java) {
			roomService.startRoom(users[2], rooms[0])
		}
	}

	@Test
	fun dropRoom_correctly() {
		Assertions.assertDoesNotThrow {
			roomService.dropRoom(rooms[0].creator, rooms[0])
		}
	}

	@Test
	fun dropRoom_userNotPermitted() {
		Assertions.assertThrows(RoomActionDeniedException::class.java) {
			roomService.dropRoom(users[2], rooms[0])
		}
	}
}
