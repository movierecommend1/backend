package io.movierecommend.backend.services

import io.movierecommend.backend.documents.User
import io.movierecommend.backend.dto.UserDTO
import io.movierecommend.backend.exceptions.InvalidPasswordException
import io.movierecommend.backend.exceptions.UsernameExistsException
import io.movierecommend.backend.repositories.UserRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@AutoConfigureMockMvc
class UserServiceTests {
	@Autowired
	private lateinit var userService: UserService

	@Autowired
	private lateinit var passwordEncoder: PasswordEncoder

	@MockBean
	private lateinit var userRepository: UserRepository

	@BeforeEach
	fun setup() {
		val users = mutableListOf(
			User(
				username = "baz",
				password = passwordEncoder.encode("wok")
			),
		)

		Mockito
			.`when`(userRepository.findByUsername(anyString()))
			.then { invocation ->
				users.firstOrNull { it.username == invocation.arguments.first() }
			}
		Mockito
			.`when`(userRepository.existsByUsername(anyString()))
			.then { invocation ->
				users.any { it.username == invocation.arguments.first() }
			}

		Mockito
			.`when`(userRepository.save(any(User::class.java)))
			.then { invocation ->
				users.add(invocation.arguments.first() as User)
				return@then invocation.arguments.first()
			}
	}

	@Test
	fun registerUser_valid() {
		val dto = UserDTO(username = "foo", password = "bar")

		val user = userService.registerUser(dto)

		Assertions.assertEquals(dto.username, user.username)
		Assertions.assertTrue { passwordEncoder.matches(dto.password, user.password) }
	}

	@Test
	fun registerUser_alreadyExists() {
		val dto = UserDTO(
			username = "baz",
			password = "0000"
		)

		Assertions.assertThrows(UsernameExistsException::class.java) {
			userService.registerUser(dto)
		}
	}

	@Test
	fun existsByUsername_exists() {
		Assertions.assertTrue { userService.usernameExists("baz") }
	}

	@Test
	fun existsByUsername_dontExist() {
		Assertions.assertFalse { userService.usernameExists("foo") }
	}

	@Test
	fun getUser_exists() {
		val user = userService.getUser("baz")

		Assertions.assertInstanceOf(User::class.java, user)
	}

	@Test
	fun getUser_dontExist() {
		Assertions.assertThrows(UsernameNotFoundException::class.java) {
			userService.getUser("foo")
		}
	}

	@Test
	fun verifyUser_exists() {
		val dto = UserDTO(
			username = "baz",
			password = "wok"
		)

		Assertions.assertInstanceOf(User::class.java, userService.verifyUser(dto))
	}

	@Test
	fun verifyUser_dontExist() {
		val dto = UserDTO(
			username = "foo",
			password = "123"
		)

		Assertions.assertThrows(UsernameNotFoundException::class.java) {
			userService.verifyUser(dto)
		}
	}

	@Test
	fun verifyUser_wrongPassword() {
		val dto = UserDTO(
			username = "baz",
			password = "123"
		)

		Assertions.assertThrows(InvalidPasswordException::class.java) {
			userService.verifyUser(dto)
		}
	}
}
