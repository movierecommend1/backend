package io.movierecommend.backend.controllers

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.dto.UserDTO
import io.movierecommend.backend.services.UserService
import io.movierecommend.backend.services.auth.AuthService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTests {
	@Autowired
	private lateinit var userController: UserController

	@MockBean
	private lateinit var userService: UserService

	@MockBean
	private lateinit var authService: AuthService

	private val token = "thisisatoken"

	private val user = User("abap", "abap")
		.also { it.id = ObjectId.get() }

	private val userDTO = UserDTO(
		username = user.username,
		password = user.password
	)

	@BeforeEach
	fun setup() {
		Mockito
			.`when`(userService.registerUser(MockitoKotlinHelper.anyObject()))
			.thenReturn(user)
		Mockito
			.`when`(userService.verifyUser(MockitoKotlinHelper.anyObject()))
			.thenReturn(user)
		Mockito
			.`when`(authService.generateToken(MockitoKotlinHelper.anyObject()))
			.thenReturn(token)
	}

	@Test
	fun register() {
		val tok = userController.register(userDTO)

		Assertions.assertEquals(token, tok.token)
	}

	@Test
	fun login() {
		val tok = userController.login(userDTO)

		Assertions.assertEquals(token, tok.token)
	}
}
