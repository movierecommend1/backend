package io.movierecommend.backend.controllers

import io.movierecommend.backend.MockitoKotlinHelper
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.dto.MovieIdDTO
import io.movierecommend.backend.exceptions.NoMovieException
import io.movierecommend.backend.exceptions.NoRoomException
import io.movierecommend.backend.exceptions.NotInRoomException
import io.movierecommend.backend.services.MovieService
import io.movierecommend.backend.services.RoomActionsService
import io.movierecommend.backend.services.RoomService
import io.movierecommend.backend.services.UserService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.security.Principal

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RoomControllerTests {
	@Autowired
	private lateinit var roomController: RoomController

	@MockBean
	private lateinit var actionsService: RoomActionsService

	@MockBean
	private lateinit var userService: UserService

	@MockBean
	private lateinit var roomService: RoomService

	@MockBean
	private lateinit var movieService: MovieService

	private val user = User("abap", "abap")
		.also { it.id = ObjectId.get() }

	private val room = Room(
		slug = "AAA",
		creator = user,
		users = listOf(user),
	).also { it.id = ObjectId.get() }

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	val principal = Principal { user.username }

	@BeforeEach
	fun setup() {
		Mockito
			.`when`(userService.getUser(anyString()))
			.then { user }
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(room)
		Mockito
			.`when`(roomService.joinRoom(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject()))
			.thenReturn(room)
		Mockito
			.`when`(roomService.startRoom(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject()))
			.thenReturn(room)
		Mockito
			.`when`(actionsService.recommend(MockitoKotlinHelper.anyObject(), MockitoKotlinHelper.anyObject()))
			.thenReturn(movie)
		Mockito
			.`when`(movieService.getMovieById(anyString()))
			.thenReturn(movie)
	}

	@Test
	fun create_creates() {
		Mockito
			.`when`(roomService.createRoom(user))
			.thenReturn(room)

		val created = roomController.create(principal)

		Assertions.assertEquals(room.slug, created.slug)
		Assertions.assertEquals(room.status, created.status)
		Assertions.assertEquals(room.creator.username, created.creator)
	}

	@Test
	fun roomInfo_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.roomInfo("ABAP", principal)
		}
	}

	@Test
	fun roomInfo_notAllowed() {
		Mockito
			.`when`(userService.getUser(MockitoKotlinHelper.anyObject()))
			.thenReturn(
				User(username = "foobar", password = "1234")
					.also { it.id = ObjectId.get() }
			)

		Assertions.assertThrows(NotInRoomException::class.java) {
			roomController.roomInfo("ABAP", principal)
		}
	}

	@Test
	fun roomInfo_succeeds() {
		val info = roomController.roomInfo(room.slug, principal)

		Assertions.assertEquals(room.slug, info.slug)
		Assertions.assertEquals(room.status, info.status)
		Assertions.assertEquals(room.creator.username, info.creator)
	}

	@Test
	fun roomStats_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.roomStats("ABAP", principal)
		}
	}

	@Test
	fun roomStats_notAllowed() {
		Mockito
			.`when`(userService.getUser(MockitoKotlinHelper.anyObject()))
			.thenReturn(
				User(username = "foobar", password = "1234")
					.also { it.id = ObjectId.get() }
			)

		Assertions.assertThrows(NotInRoomException::class.java) {
			roomController.roomStats("ABAP", principal)
		}
	}

	@Test
	fun roomStats_succeeds() {
		roomController.roomStats(room.slug, principal)

		Mockito.verify(actionsService).getRoomStats(room)
	}

	@Test
	fun join_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.join("ABAP", principal)
		}
	}

	@Test
	fun join_succeeds() {
		roomController.join(room.slug, principal)

		Mockito.verify(roomService).joinRoom(user, room)
	}

	@Test
	fun start_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.start("ABAP", principal)
		}
	}

	@Test
	fun start_succeeds() {
		roomController.start(room.slug, principal)

		Mockito.verify(roomService).startRoom(user, room)
	}

	@Test
	fun recommend_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.recommend("ABAP", principal)
		}
	}

	@Test
	fun recommend_succeeds() {
		roomController.recommend(room.slug, principal)

		Mockito.verify(actionsService).recommend(user, room)
	}

	val movieId = MovieIdDTO(
		movieId = ObjectId.get().toHexString()
	)

	@Test
	fun like_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.like("ABAP", movieId, principal)
		}
	}

	@Test
	fun like_noSuchMovie() {
		Mockito
			.`when`(movieService.getMovieById(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoMovieException::class.java) {
			roomController.like("ABAP", movieId, principal)
		}
	}

	@Test
	fun like_succeeds() {
		roomController.like(room.slug, movieId, principal)

		Mockito.verify(actionsService).likeMovie(user, room, movie)
	}

	@Test
	fun drop_noSuchRoom() {
		Mockito
			.`when`(roomService.getRoomBySlug(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoRoomException::class.java) {
			roomController.drop("ABAP", principal)
		}
	}

	@Test
	fun drop_succeeds() {
		roomController.drop(room.slug, principal)

		Mockito.verify(roomService).dropRoom(user, room)
	}
}
