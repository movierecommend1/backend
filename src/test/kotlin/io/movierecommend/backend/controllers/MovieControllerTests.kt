package io.movierecommend.backend.controllers

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.dto.MovieDTO
import io.movierecommend.backend.exceptions.NoMovieException
import io.movierecommend.backend.services.MovieService
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MovieControllerTests {
	@Autowired
	private lateinit var movieController: MovieController

	@MockBean
	private lateinit var movieService: MovieService

	private val movie = Movie(
		primaryName = "A Movie",
		enName = null,
		description = "This is a movie",
		poster = Movie.Poster(
			previewUrl = "http://preview",
			url = "http://fullimage"
		),
		rating = Movie.Rating(
			kinopoisk = 3.0,
			imdb = 3.0,
			tmdb = null
		),
		internalGenres = listOf(),
		persons = listOf(),
		names = listOf(Movie.Name("Фильм"))
	).also { it.id = ObjectId.get() }

	@Test
	fun getMovie_noMovie() {
		Mockito
			.`when`(movieService.getMovieById(anyString()))
			.thenReturn(null)

		Assertions.assertThrows(NoMovieException::class.java) {
			movieController.getMovie(movie.id.toHexString())
		}
	}

	@Test
	fun getMovie_givesMovie() {
		Mockito
			.`when`(movieService.getMovieById(anyString()))
			.thenReturn(movie)

		val movieInfo = movieController.getMovie(movie.id.toHexString())

		Assertions.assertEquals(movie.id.toHexString(), movieInfo.id)
		Assertions.assertEquals(movie.name, movieInfo.name)
		Assertions.assertEquals(movie.rating, movieInfo.rating)
		Assertions.assertIterableEquals(
			movie.actors.map { MovieDTO.ActorDTO(name = it.name, photoUrl = it.photoUrl) },
			movieInfo.actors
		)
		Assertions.assertEquals(movie.description, movieInfo.description)
		Assertions.assertIterableEquals(movie.genres, movieInfo.genres)
		Assertions.assertEquals(movie.poster.url, movieInfo.posterUrl)
	}
}
