package io.movierecommend.backend

import org.mockito.Mockito

object MockitoKotlinHelper {
	fun <T> anyObject(): T = Mockito.any()
}
