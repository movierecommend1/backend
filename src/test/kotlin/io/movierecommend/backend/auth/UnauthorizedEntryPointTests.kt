package io.movierecommend.backend.auth

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.security.core.AuthenticationException
import org.springframework.test.context.junit.jupiter.SpringExtension
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UnauthorizedEntryPointTests {
	private lateinit var unauthorizedEntryPoint: UnauthorizedEntryPoint

	@BeforeEach
	fun setup() {
		unauthorizedEntryPoint = UnauthorizedEntryPoint()
	}

	@Test
	fun commence_requestNull() {
		val servletResponse = Mockito.mock(HttpServletResponse::class.java)

		Assertions.assertThrows(IllegalStateException::class.java) {
			unauthorizedEntryPoint.commence(null, servletResponse, null)
		}
	}

	@Test
	fun commence_responseNull() {
		val servletRequest = Mockito.mock(HttpServletRequest::class.java)

		Assertions.assertThrows(IllegalStateException::class.java) {
			unauthorizedEntryPoint.commence(servletRequest, null, null)
		}
	}

	@Test
	fun commence_correctly() {
		val servletRequest = Mockito.mock(HttpServletRequest::class.java)
		val servletResponse = Mockito.mock(HttpServletResponse::class.java)
		val authException = Mockito.mock(AuthenticationException::class.java)

		val message = "message"

		Mockito
			.`when`(authException.message)
			.thenReturn(message)

		unauthorizedEntryPoint.commence(servletRequest, servletResponse, authException)

		Mockito.verify(servletResponse)
			.sendError(HttpStatus.UNAUTHORIZED.value(), message)
	}
}
