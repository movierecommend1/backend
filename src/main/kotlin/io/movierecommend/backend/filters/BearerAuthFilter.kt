package io.movierecommend.backend.filters

import io.movierecommend.backend.services.auth.AuthService
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class BearerAuthFilter(
	private val authService: AuthService
) : OncePerRequestFilter() {
	companion object {
		const val TOKEN_HEADER_PREFIX = "Bearer "
	}

	override fun doFilterInternal(
		request: HttpServletRequest,
		response: HttpServletResponse,
		filterChain: FilterChain
	) {
		val tokenHeader = request.getHeader("Authorization")

		if (isTokenValid(tokenHeader)) {
			SecurityContextHolder
				.getContext()
				.authentication = extractToken(tokenHeader)
		}

		filterChain.doFilter(request, response)
	}

	private fun isTokenValid(tokenHeader: String?) =
		tokenHeader?.startsWith(TOKEN_HEADER_PREFIX) ?: false

	private fun extractToken(tokenHeader: String) =
		authService.parseToken(
			tokenHeader.replace(TOKEN_HEADER_PREFIX, "")
		).let {
			UsernamePasswordAuthenticationToken(
				it.username,
				null,
				listOf()
			)
		}
}
