package io.movierecommend.backend.exceptions

import java.io.Serializable

sealed class AppException(
	message: String
) : Exception(message), Serializable
