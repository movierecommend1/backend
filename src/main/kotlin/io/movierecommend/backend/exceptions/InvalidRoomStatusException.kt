package io.movierecommend.backend.exceptions

import io.movierecommend.backend.documents.Room
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class InvalidRoomStatusException(
	status: Room.Status,
	operation: Operation,
) : AppException(
	message = "Cannot ${operation.name.lowercase()} a room with status '${status.name}'!"
) {
	enum class Operation {
		Join,
		Start
	}
}
