package io.movierecommend.backend.exceptions

class NotInRoomException(
	slug: String,
	username: String
) : AppException(
	message = "$username is not in room $slug!"
)
