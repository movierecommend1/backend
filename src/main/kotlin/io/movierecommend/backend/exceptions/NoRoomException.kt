package io.movierecommend.backend.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class NoRoomException(
	slug: String
) : AppException(
	message = "No such room: $slug!"
)
