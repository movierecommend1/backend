package io.movierecommend.backend.exceptions

class NoMoviesLeftException : AppException(
	message = "No movies left in the database!"
)
