package io.movierecommend.backend.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class NoMovieException(
	id: String
) : AppException(
	message = "No movie with ID '$id'!"
)
