package io.movierecommend.backend.exceptions

class AlreadyInRoomException(
	roomSlugs: List<String>
) : AppException(
	message = "You are already registered in room(s): ${roomSlugs.joinToString(", ")}"
)
