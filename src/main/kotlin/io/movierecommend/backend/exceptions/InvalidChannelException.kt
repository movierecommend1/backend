package io.movierecommend.backend.exceptions

class InvalidChannelException(
	destination: String
) : AppException(
	message = "No channel at '$destination'!"
)
