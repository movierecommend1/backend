package io.movierecommend.backend.exceptions

class RoomActionDeniedException(
	action: Action
) : AppException(
	message = "Only creator of the room can ${actionToString(action)} it!"
) {

	companion object {
		private fun actionToString(action: Action) =
			when (action) {
				Action.Start -> "start"
				Action.Drop -> "drop"
			}
	}

	enum class Action {
		Start,
		Drop
	}
}
