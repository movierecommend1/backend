package io.movierecommend.backend.documents

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef

data class UserMovieAppearance(
	@DBRef val user: User,
	@DBRef val movie: Movie,
	@DBRef val room: Room,
	val liked: Boolean
) {
	@Id
	lateinit var id: ObjectId
}
