package io.movierecommend.backend.documents

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Field

data class Movie(
	@Field("name")
	private val primaryName: String?,
	private val enName: String?,
	val description: String,
	val poster: Poster,
	val rating: Rating,
	@Field("genres")
	private val internalGenres: List<Genre>?,
	private val persons: List<Person>,
	private val names: List<Name>
) {
	data class Poster(
		val previewUrl: String,
		val url: String
	)

	data class Person(
		@Field("enProfession")
		val position: String,
		@Field("name")
		private val primaryName: String?,
		private val enName: String?,
		@Field("photo")
		val photoUrl: String
	) {
		val name: String?
			get() = primaryName ?: enName
	}

	data class Rating(
		@Field("kp")
		val kinopoisk: Double?,
		val imdb: Double?,
		val tmdb: Double?
	)

	data class Genre(
		val name: String
	)

	data class Name(
		val name: String?,
	)

	@Id
	lateinit var id: ObjectId

	val genres: List<String>
		get() = (internalGenres ?: listOf()).map { it.name }

	val actors: List<Person>
		get() = persons.filter { it.position == "actor" }

	val name: String
		get() = primaryName ?: enName ?: alternateNames.first()

	private val alternateNames: List<String>
		get() = names.mapNotNull { it.name }
}
