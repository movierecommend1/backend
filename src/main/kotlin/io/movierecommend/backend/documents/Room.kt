package io.movierecommend.backend.documents

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.MongoId

data class Room(
	val slug: String,
	@DBRef val creator: User,
	@DBRef val users: List<User>,
	val status: Status = Status.Created,
) {
	enum class Status {
		Created,
		Started,
		Abandoned
	}

	@MongoId
	lateinit var id: ObjectId
}
