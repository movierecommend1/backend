package io.movierecommend.backend.documents

import org.bson.types.ObjectId

data class UserPrincipal(
	val id: ObjectId,
	val username: String
)
