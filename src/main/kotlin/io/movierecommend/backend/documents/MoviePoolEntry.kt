package io.movierecommend.backend.documents

import org.springframework.data.mongodb.core.mapping.DBRef

data class MoviePoolEntry(
	@DBRef val movie: Movie,
	@DBRef val room: Room
)
