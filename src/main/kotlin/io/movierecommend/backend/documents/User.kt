package io.movierecommend.backend.documents

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

@Document
data class User(
	private val username: String,
	private val password: String,
	val flags: UserFlags = UserFlags(),
) : UserDetails {
	@MongoId
	lateinit var id: ObjectId

	data class UserFlags(
		val expired: Boolean = false,
		val locked: Boolean = false,
		val enabled: Boolean = false
	)

	override fun getAuthorities() = mutableListOf<GrantedAuthority>()

	override fun getPassword() = password

	override fun getUsername() = username

	override fun isAccountNonExpired() = !flags.expired
	override fun isAccountNonLocked() = !flags.locked

	override fun isCredentialsNonExpired() = true

	override fun isEnabled() = flags.enabled
}
