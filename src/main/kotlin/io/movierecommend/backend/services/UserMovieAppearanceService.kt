package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserMovieAppearance
import io.movierecommend.backend.repositories.UserMovieAppearanceRepository
import org.springframework.stereotype.Service

@Service
class UserMovieAppearanceService(
	private val appearanceRepository: UserMovieAppearanceRepository
) {
	fun getAllForRoom(room: Room) =
		appearanceRepository.findAllByRoomId(room.id)

	fun didUserSeeMovie(room: Room, user: User, movie: Movie) =
		appearanceRepository.find(user.id, movie.id, room.id) != null

	fun getAllLikedForUser(room: Room, user: User) =
		appearanceRepository.findLikedByRoomAndUserId(room.id, user.id)

	fun markMovieSeen(room: Room, user: User, movie: Movie, liked: Boolean = false) =
		appearanceRepository.find(user.id, movie.id, room.id)
			.let { appearance ->
				if (appearance == null) {
					appearanceRepository.save(
						UserMovieAppearance(user, movie, room, liked)
					)
				} else {
					appearanceRepository.save(
						appearance.copy(liked = true).also { it.id = appearance.id }
					)
				}
			}

	fun isMovieAMatch(room: Room, movie: Movie) =
		appearanceRepository.findAllByRoomAndMovieId(room.id, movie.id)
			.filter { it.liked }
			.map { it.user.id }
			.let { liked ->
				room.users
					.map { it.id }
					.all { it in liked }
			}
}
