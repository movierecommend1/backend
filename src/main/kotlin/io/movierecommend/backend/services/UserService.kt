package io.movierecommend.backend.services

import io.movierecommend.backend.documents.User
import io.movierecommend.backend.dto.UserDTO
import io.movierecommend.backend.exceptions.InvalidPasswordException
import io.movierecommend.backend.exceptions.UsernameExistsException
import io.movierecommend.backend.repositories.UserRepository
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService(
	private val userRepository: UserRepository,
	private val passwordEncoder: PasswordEncoder
) {
	fun usernameExists(username: String) =
		userRepository.existsByUsername(username)

	fun getUser(username: String) =
		findUser(username) ?: throw UsernameNotFoundException("Username does not exist!")

	fun findUser(username: String) =
		userRepository.findByUsername(username)

	fun registerUser(userDTO: UserDTO): User {
		if (usernameExists(userDTO.username)) throw UsernameExistsException()

		return userRepository.save(
			buildUser(userDTO)
		)
	}

	fun verifyUser(userDTO: UserDTO) =
		(
			findUser(userDTO.username)
				?: throw UsernameNotFoundException("Username does not exist!")
			)
			.also {
				if (
					!passwordEncoder.matches(userDTO.password, it.password)
				) throw InvalidPasswordException()
			}

	private fun buildUser(userDTO: UserDTO) = User(
		username = userDTO.username,
		password = passwordEncoder.encode(userDTO.password)
	)
}
