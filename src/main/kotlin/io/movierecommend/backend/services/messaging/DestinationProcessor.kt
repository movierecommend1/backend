package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.documents.Room
import org.springframework.stereotype.Service

@Service
class DestinationProcessor {
	enum class DestinationType {
		START,
		MATCH
	}

	data class ParsedDestination(
		val roomSlug: String,
		val type: DestinationType
	)

	fun composeDestination(room: Room, destinationType: DestinationType) =
		"/${typeToRoutePart(destinationType)}/${room.id.toHexString()}"

	fun parseDestination(destination: String): ParsedDestination? {
		val parts = destination.split("/")

		if (parts.size != 3) return null

		if (parts[0] != "") return null

		val type = routePartToType(parts[1]) ?: return null

		return ParsedDestination(
			roomSlug = parts[2],
			type
		)
	}

	private fun typeToRoutePart(destinationType: DestinationType) =
		when (destinationType) {
			DestinationType.MATCH -> "match"
			DestinationType.START -> "start"
		}

	private fun routePartToType(part: String): DestinationType? =
		when (part) {
			"match" -> DestinationType.MATCH
			"start" -> DestinationType.START
			else -> null
		}
}
