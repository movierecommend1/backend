package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.events.MovieMatchedEvent
import io.movierecommend.backend.events.RoomStartedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class MessageListener(
	private val messagingService: MessagingService
) {
	@EventListener
	fun onMovieMatched(event: MovieMatchedEvent) = messagingService.sendMatchNotification(
		movie = event.movie,
		room = event.room
	)

	@EventListener
	fun onRoomStarted(event: RoomStartedEvent) = messagingService.sendStartNotification(event.room)
}
