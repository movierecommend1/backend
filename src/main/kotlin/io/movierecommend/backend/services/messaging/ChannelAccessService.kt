package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.documents.UserPrincipal
import io.movierecommend.backend.exceptions.InvalidChannelException
import io.movierecommend.backend.exceptions.NoRoomException
import io.movierecommend.backend.services.RoomService
import org.springframework.stereotype.Service

@Service
class ChannelAccessService(
	private val roomService: RoomService,
	private val destinationProcessor: DestinationProcessor
) {
	fun checkSubscribeDestinationAccess(principal: UserPrincipal, destination: String): Boolean {
		val dest = destinationProcessor.parseDestination(destination) ?: throw InvalidChannelException(destination)
		val room = roomService.getRoomBySlug(dest.roomSlug) ?: throw NoRoomException(dest.roomSlug)

		return room.users.find { it.id == principal.id && it.username == principal.username } != null
	}
}
