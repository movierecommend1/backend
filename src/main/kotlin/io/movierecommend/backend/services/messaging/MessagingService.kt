package io.movierecommend.backend.services.messaging

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.messages.MatchNotification
import io.movierecommend.backend.messages.RoomStartNotification
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
class MessagingService(
	private val messagingTemplate: SimpMessagingTemplate,
	private val destinationProcessor: DestinationProcessor
) {
	fun sendMatchNotification(movie: Movie, room: Room) =
		messagingTemplate.convertAndSend(
			destinationProcessor.composeDestination(
				room,
				DestinationProcessor.DestinationType.MATCH
			),
			MatchNotification(
				movieId = movie.id.toHexString()
			)
		)

	fun sendStartNotification(room: Room) =
		messagingTemplate.convertAndSend(
			destinationProcessor.composeDestination(
				room,
				DestinationProcessor.DestinationType.START
			),
			RoomStartNotification(
				roomSlug = room.slug
			)
		)
}
