package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.dto.RoomStatsDTO
import io.movierecommend.backend.events.MovieMatchedEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class RoomActionsService(
	private val poolService: MoviePoolService,
	private val appearanceService: UserMovieAppearanceService,
	private val eventPublisher: ApplicationEventPublisher,
) {
	fun recommend(user: User, room: Room): Movie =
		poolService.pullMovieForUser(user, room)
			.also { appearanceService.markMovieSeen(room, user, it) }

	fun likeMovie(user: User, room: Room, movie: Movie) {
		appearanceService.markMovieSeen(room, user, movie, liked = true)

		if (appearanceService.isMovieAMatch(room, movie)) {
			eventPublisher.publishEvent(
				MovieMatchedEvent(
					emitter = this,
					room, movie
				)
			)
		}
	}

	fun getRoomStats(room: Room): RoomStatsDTO {
		val appearances = appearanceService.getAllForRoom(room)

		val rankMap = mutableMapOf<String, MutableSet<String>>()
		val movies = appearances.map { it.movie }.distinctBy { it.id }

		appearances.forEach {
			val forMovie = rankMap.getOrPut(it.movie.id.toHexString()) { mutableSetOf() }

			if (it.liked) {
				forMovie.add(it.user.id.toHexString())
			}
		}

		return RoomStatsDTO(
			matchedMovies = movies.filter { appearanceService.isMovieAMatch(room, it) }.map { it.id.toHexString() },
			ranking = rankMap.entries.map { (movie, users) ->
				RoomStatsDTO.Rank(
					movieId = movie,
					likedUsers = users.toList()
				)
			}
		)
	}
}
