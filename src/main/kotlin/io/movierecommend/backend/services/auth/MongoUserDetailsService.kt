package io.movierecommend.backend.services.auth

import io.movierecommend.backend.services.UserService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class MongoUserDetailsService(
	private val userService: UserService
) : UserDetailsService {
	override fun loadUserByUsername(username: String?): UserDetails =
		(username ?: throw UsernameNotFoundException("empty username!"))
			.let {
				userService.findUser(it)
					?: throw UsernameNotFoundException("no user with username!")
			}
}
