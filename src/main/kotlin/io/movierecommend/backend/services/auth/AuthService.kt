package io.movierecommend.backend.services.auth

import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserPrincipal
import org.springframework.stereotype.Service
import java.security.Principal

@Service
interface AuthService {
	fun generateToken(user: User): String

	fun parseToken(token: String): UserPrincipal

	fun createSecurityPrincipal(token: String): Principal
	fun createSecurityPrincipal(userPrincipal: UserPrincipal): Principal
}
