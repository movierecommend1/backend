package io.movierecommend.backend.services.auth

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.movierecommend.backend.config.properties.SecurityProperties
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.documents.UserPrincipal
import org.bson.types.ObjectId
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Service
import java.security.Principal
import java.time.Instant
import java.util.*

@Service
class JWTAuthService(
	private val securityProperties: SecurityProperties
) : AuthService {
	override fun generateToken(user: User): String =
		Jwts.builder()
			.claim("id", user.id.toHexString())
			.setSubject(user.username)
			.setExpiration(computeExpiryDate())
			.signWith(securityProperties.signingKey, SignatureAlgorithm.HS384)
			.compact()

	override fun parseToken(token: String) =
		Jwts.parserBuilder()
			.setSigningKey(securityProperties.signingKey)
			.build()
			.parseClaimsJws(token).body.let {
				UserPrincipal(
					id = ObjectId(it.get("id", String::class.java)),
					username = it.subject
				)
			}

	override fun createSecurityPrincipal(token: String) =
		createSecurityPrincipal(parseToken(token))

	override fun createSecurityPrincipal(userPrincipal: UserPrincipal): Principal =
		UsernamePasswordAuthenticationToken(
			userPrincipal.username,
			null,
			listOf()
		)

	private fun computeExpiryDate(): Date =
		Date.from(Instant.now().plusSeconds(securityProperties.tokenExpiry))
}
