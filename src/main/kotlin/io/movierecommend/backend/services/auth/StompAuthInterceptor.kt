package io.movierecommend.backend.services.auth

import io.jsonwebtoken.MalformedJwtException
import io.movierecommend.backend.services.UserService
import io.movierecommend.backend.services.messaging.ChannelAccessService
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.ChannelInterceptor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.stereotype.Component
import javax.naming.AuthenticationException

@Component
class StompAuthInterceptor(
	private val authService: AuthService,
	private val userService: UserService,
	private val channelAccessService: ChannelAccessService
) : ChannelInterceptor {
	companion object {
		const val TOKEN_HEADER = "x-auth-token"
	}

	override fun preSend(message: Message<*>, channel: MessageChannel): Message<*>? {
		val accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java) ?: return message

		if (accessor.command != StompCommand.SUBSCRIBE) {
			return message
		}

		val user = try {
			accessor.getFirstNativeHeader(TOKEN_HEADER).toString()
				.let { authService.parseToken(it) }
		} catch (ex: MalformedJwtException) {
			throw AuthenticationException("Invalid JWT token!")
		}

		if (!userService.usernameExists(user.username)) {
			throw AuthenticationException("Invalid token or user does not exist!")
		}

		if (!channelAccessService.checkSubscribeDestinationAccess(user, accessor.destination ?: "")) {
			throw AuthenticationException("You do not belong to this room!")
		}

		accessor.user = authService.createSecurityPrincipal(user)

		return message
	}
}
