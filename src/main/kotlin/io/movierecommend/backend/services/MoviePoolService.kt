package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.MoviePoolEntry
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.exceptions.NoMoviesLeftException
import io.movierecommend.backend.repositories.MoviePoolRepository
import io.movierecommend.backend.services.recommend.MovieRecommendService
import org.springframework.stereotype.Service

@Service
class MoviePoolService(
	private val poolRepository: MoviePoolRepository,
	private val appearanceService: UserMovieAppearanceService,
	private val recommendService: MovieRecommendService
) {
	companion object {
		const val MAX_RETRIES = 3
	}

	fun pullMovieForUser(user: User, room: Room, refill: Boolean = true) =
		if (refill) {
			pullMovieForUserOrRefill(user, room)
		} else {
			pullMovieForUser(user, room) ?: throw NoMoviesLeftException()
		}

	fun getPoolContents(room: Room) =
		poolRepository.findAllByRoomId(room.id)

	private fun pullMovieForUser(user: User, room: Room): Movie? {
		val pool = getPoolContents(room).filterNot { appearanceService.didUserSeeMovie(room, user, it.movie) }

		return if (pool.isNotEmpty()) {
			pool.random().movie
		} else {
			null
		}
	}

	private fun pullMovieForUserOrRefill(user: User, room: Room): Movie {
		repeat(MAX_RETRIES) {
			val movie = pullMovieForUser(user, room)

			if (movie != null) return movie

			refillPool(room, user)
		}

		throw NoMoviesLeftException()
	}

	private fun refillPool(room: Room, user: User) {
		val liked = appearanceService.getAllLikedForUser(room, user).map { it.movie }
		val seen = getPoolContents(room).map { it.movie }

		recommendService.next(liked, seen)
			.map {
				MoviePoolEntry(
					movie = it,
					room
				)
			}
			.forEach { poolRepository.save(it) }
	}
}
