package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.repositories.MovieRepository
import org.bson.types.ObjectId
import org.springframework.stereotype.Service

@Service
class MovieService(
	private val movieRepository: MovieRepository
) {
	fun getMovieById(id: String): Movie? =
		movieRepository.findById(ObjectId(id)).orElseGet { null }

	fun streamAllExceptIds(ids: List<ObjectId>) =
		movieRepository.findAllByIdNotIn(ids)
}
