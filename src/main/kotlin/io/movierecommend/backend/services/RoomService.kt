package io.movierecommend.backend.services

import io.movierecommend.backend.config.properties.RoomsProperties
import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.events.RoomStartedEvent
import io.movierecommend.backend.exceptions.AlreadyInRoomException
import io.movierecommend.backend.exceptions.InvalidRoomStatusException
import io.movierecommend.backend.exceptions.RoomActionDeniedException
import io.movierecommend.backend.repositories.RoomRepository
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class RoomService(
	private val roomRepository: RoomRepository,
	private val roomsProperties: RoomsProperties,
	private val eliminator: RoomEliminator,
	private val slugGenerator: SlugGenerator,
	private val eventPublisher: ApplicationEventPublisher
) {
	fun getRoomsByUser(user: User) =
		roomRepository.findAllByUserId(user.id)

	fun getRoomBySlug(slug: String) =
		roomRepository.findBySlug(slug)

	fun createRoom(creator: User) =
		roomRepository.save(
			Room(
				slug = generateUniqueSlug(),
				creator = creator,
				users = mutableListOf(creator)
			)
		)

	fun joinRoom(joiner: User, room: Room): Room {
		if (room.status != Room.Status.Created) {
			throw InvalidRoomStatusException(room.status, InvalidRoomStatusException.Operation.Join)
		}

		val userRooms = getRoomsByUser(joiner).filter { it.status != Room.Status.Abandoned }

		if (userRooms.isNotEmpty()) {
			throw AlreadyInRoomException(userRooms.map { it.slug })
		}

		return roomRepository.save(room.copy(users = room.users + listOf(joiner)).also { it.id = room.id })
	}

	fun startRoom(starter: User, room: Room): Room {
		if (room.status != Room.Status.Created) {
			throw InvalidRoomStatusException(room.status, InvalidRoomStatusException.Operation.Start)
		}

		if (room.creator.id != starter.id) {
			throw RoomActionDeniedException(RoomActionDeniedException.Action.Start)
		}

		return roomRepository.save(room.copy(status = Room.Status.Started).also { it.id = room.id })
			.also { eventPublisher.publishEvent(RoomStartedEvent(emitter = this, room = it)) }
	}

	fun dropRoom(actor: User, room: Room) {
		if (room.creator.id != actor.id) {
			throw RoomActionDeniedException(RoomActionDeniedException.Action.Drop)
		}

		eliminator.eliminate(room)
	}

	private fun generateUniqueSlug() =
		generateSequence { slugGenerator.generate(roomsProperties.slugLength) }
			.first { !roomRepository.existsBySlug(it) }
}
