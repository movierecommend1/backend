package io.movierecommend.backend.services.recommend

import com.fasterxml.jackson.databind.ObjectMapper
import io.movierecommend.backend.config.properties.RecommendProperties
import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.services.MovieService
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.web.util.UriComponentsBuilder

class MicroMovieRecommendService(
	properties: RecommendProperties,
	restTemplateBuilder: RestTemplateBuilder,
	private val objectMapper: ObjectMapper,
	private val movieService: MovieService
) : MovieRecommendService {
	private val restTemplate = restTemplateBuilder
		.rootUri(properties.baseUrl)
		.build()

	override fun next(likedMovies: List<Movie>, seenMovies: List<Movie>): List<Movie> =
		sendMicroRequest(
			likedMovies = likedMovies.map { it.id.toHexString() },
			excludeMovies = seenMovies.map { it.id.toHexString() }
		).mapNotNull { movieService.getMovieById(it) }

	private fun sendMicroRequest(
		likedMovies: List<String>,
		excludeMovies: List<String>
	) = restTemplate.exchange(
		uriTemplate(),
		HttpMethod.GET,
		HttpEntity.EMPTY,
		object : ParameterizedTypeReference<List<String>>() {},
		mapOf(
			"likedMovies" to objectMapper.writeValueAsString(likedMovies),
			"excludeMovies" to objectMapper.writeValueAsString(excludeMovies),
		)
	).body ?: throw IllegalStateException("micro-recommend request failed!")

	private fun uriTemplate() =
		UriComponentsBuilder
			.fromUriString("/get_recommendation")
			.queryParam("recommend_on", "{likedMovies}")
			.queryParam("movie_exceptions", "{excludeMovies}")
			.encode()
			.toUriString()
}
