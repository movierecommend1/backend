package io.movierecommend.backend.services.recommend

import io.movierecommend.backend.documents.Movie
import org.springframework.stereotype.Service

@Service
interface MovieRecommendService {
	fun next(likedMovies: List<Movie>, seenMovies: List<Movie>): List<Movie>
}
