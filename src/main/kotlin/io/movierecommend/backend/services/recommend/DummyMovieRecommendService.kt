package io.movierecommend.backend.services.recommend

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.services.MovieService
import kotlin.streams.toList

class DummyMovieRecommendService(
	private val movieService: MovieService
) : MovieRecommendService {

	companion object {
		const val RECOMMEND_LIMIT = 8L
	}

	override fun next(likedMovies: List<Movie>, seenMovies: List<Movie>): List<Movie> {
		return movieService.streamAllExceptIds(seenMovies.map { it.id })
			.limit(RECOMMEND_LIMIT)
			.toList()
	}
}
