package io.movierecommend.backend.services

import org.springframework.stereotype.Service
import java.security.SecureRandom

@Service
class SlugGenerator {
	companion object {
		private const val charMap = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	}

	private val random = SecureRandom()

	fun generate(length: Int) =
		List(length) { charMap[nextIndex()] }
			.joinToString("")

	private fun nextIndex() =
		random.nextInt(charMap.length)
}
