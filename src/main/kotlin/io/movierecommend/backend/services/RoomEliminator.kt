package io.movierecommend.backend.services

import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.repositories.MoviePoolRepository
import io.movierecommend.backend.repositories.RoomRepository
import io.movierecommend.backend.repositories.UserMovieAppearanceRepository
import org.springframework.stereotype.Service

@Service
class RoomEliminator(
	private val roomRepository: RoomRepository,
	private val poolRepository: MoviePoolRepository,
	private val appearanceRepository: UserMovieAppearanceRepository
) {
	fun eliminate(room: Room) {
		poolRepository.deleteAllByRoomId(room.id)
		appearanceRepository.deleteAllByRoomId(room.id)

		roomRepository.delete(room)
	}
}
