package io.movierecommend.backend.messages

data class MatchNotification(
	val movieId: String,
)
