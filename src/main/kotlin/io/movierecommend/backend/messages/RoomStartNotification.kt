package io.movierecommend.backend.messages

data class RoomStartNotification(
	val roomSlug: String
)
