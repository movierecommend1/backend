package io.movierecommend.backend.events

import io.movierecommend.backend.documents.Movie
import io.movierecommend.backend.documents.Room
import org.springframework.context.ApplicationEvent

class MovieMatchedEvent(
	emitter: Any,
	val room: Room,
	val movie: Movie
) : ApplicationEvent(emitter)
