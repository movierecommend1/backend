package io.movierecommend.backend.events

import io.movierecommend.backend.documents.Room
import org.springframework.context.ApplicationEvent

class RoomStartedEvent(
	emitter: Any,
	val room: Room
) : ApplicationEvent(emitter)
