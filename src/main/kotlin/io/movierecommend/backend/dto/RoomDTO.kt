package io.movierecommend.backend.dto

import io.movierecommend.backend.documents.Room

data class RoomDTO(
	val id: String,
	val slug: String,
	val creator: String,
	val users: List<String>,
	val status: Room.Status
) {
	companion object {
		fun create(room: Room) = RoomDTO(
			id = room.id.toHexString(),
			slug = room.slug,
			creator = room.creator.username,
			users = room.users.map { it.username },
			status = room.status
		)
	}
}
