package io.movierecommend.backend.dto

data class UserDTO(
	val username: String,
	val password: String
)
