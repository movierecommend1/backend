package io.movierecommend.backend.dto

data class RoomStatsDTO(
	val matchedMovies: List<String>,
	val ranking: List<Rank>
) {
	data class Rank(
		val movieId: String,
		val likedUsers: List<String>
	)
}
