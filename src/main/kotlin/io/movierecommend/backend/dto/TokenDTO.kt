package io.movierecommend.backend.dto

data class TokenDTO(
	val token: String
)
