package io.movierecommend.backend.dto

data class MovieIdDTO(
	val movieId: String
)
