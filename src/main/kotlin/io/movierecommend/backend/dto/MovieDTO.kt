package io.movierecommend.backend.dto

import io.movierecommend.backend.documents.Movie
import io.swagger.v3.oas.annotations.media.Schema

@Schema
data class MovieDTO(
	val id: String,
	val name: String,
	val posterUrl: String,
	val description: String,
	val rating: Movie.Rating,
	val genres: List<String>,
	val actors: List<ActorDTO>
) {
	data class ActorDTO(
		val name: String?,
		val photoUrl: String
	)

	companion object {
		fun create(movie: Movie) = MovieDTO(
			id = movie.id.toHexString(),
			name = movie.name,
			posterUrl = movie.poster.url,
			description = movie.description,
			rating = movie.rating,
			genres = movie.genres,
			actors = movie.actors.map {
				ActorDTO(
					name = it.name,
					photoUrl = it.photoUrl
				)
			}
		)
	}
}
