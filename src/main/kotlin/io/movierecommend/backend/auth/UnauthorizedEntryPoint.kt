package io.movierecommend.backend.auth

import org.springframework.http.HttpStatus
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class UnauthorizedEntryPoint : AuthenticationEntryPoint {
	override fun commence(
		servletRequest: HttpServletRequest?,
		servletResponse: HttpServletResponse?,
		authException: AuthenticationException?
	) {
		checkNotNull(servletResponse) { "servletResponse is null!" }
		checkNotNull(authException) { "authException is null!" }

		servletResponse.sendError(HttpStatus.UNAUTHORIZED.value(), authException.message)
	}
}
