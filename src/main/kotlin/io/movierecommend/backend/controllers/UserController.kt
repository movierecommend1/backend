package io.movierecommend.backend.controllers

import io.movierecommend.backend.dto.TokenDTO
import io.movierecommend.backend.dto.UserDTO
import io.movierecommend.backend.services.UserService
import io.movierecommend.backend.services.auth.AuthService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/user")
@Tag(name = "User actions")
class UserController(
	private val userService: UserService,
	private val authService: AuthService
) {
	@PostMapping("/register", consumes = ["application/json"])
	fun register(@RequestBody userDTO: UserDTO) = TokenDTO(
		authService.generateToken(
			userService.registerUser(userDTO)
		)
	)

	@PostMapping("/login", consumes = ["application/json"])
	fun login(@RequestBody userDTO: UserDTO) = TokenDTO(
		authService.generateToken(
			userService.verifyUser(userDTO)
		)
	)
}
