package io.movierecommend.backend.controllers

import io.movierecommend.backend.dto.MovieDTO
import io.movierecommend.backend.exceptions.NoMovieException
import io.movierecommend.backend.services.MovieService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/movies")
@Tag(name = "Movie actions")
@SecurityRequirement(name = "moviebackend-bearer")
class MovieController(
	private val movieService: MovieService
) {
	@GetMapping("/{movieId}")
	fun getMovie(@PathVariable movieId: String) =
		MovieDTO.create(
			movieService.getMovieById(movieId)
				?: throw NoMovieException(movieId)
		)
}
