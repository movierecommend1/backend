package io.movierecommend.backend.controllers

import io.movierecommend.backend.documents.Room
import io.movierecommend.backend.documents.User
import io.movierecommend.backend.dto.MovieDTO
import io.movierecommend.backend.dto.MovieIdDTO
import io.movierecommend.backend.dto.RoomDTO
import io.movierecommend.backend.dto.RoomStatsDTO
import io.movierecommend.backend.exceptions.NoMovieException
import io.movierecommend.backend.exceptions.NoRoomException
import io.movierecommend.backend.exceptions.NotInRoomException
import io.movierecommend.backend.services.MovieService
import io.movierecommend.backend.services.RoomActionsService
import io.movierecommend.backend.services.RoomService
import io.movierecommend.backend.services.UserService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/room")
@Tag(name = "Room actions")
@SecurityRequirement(name = "moviebackend-bearer")
class RoomController(
	private val actionsService: RoomActionsService,
	private val userService: UserService,
	private val roomService: RoomService,
	private val movieService: MovieService
) {

	@PostMapping("/create")
	fun create(principal: Principal) = RoomDTO.create(
		roomService.createRoom(
			creator = userService.getUser(principal.name)
		)
	)

	@GetMapping("{roomSlug}/info")
	fun roomInfo(@PathVariable roomSlug: String, principal: Principal): RoomDTO {
		val room = ensureRoom(roomSlug)
		val user = userService.getUser(principal.name)

		if (!isUserInRoom(room, user)) {
			throw NotInRoomException(room.slug, user.username)
		}

		return RoomDTO.create(room)
	}

	@GetMapping("{roomSlug}/stats")
	fun roomStats(@PathVariable roomSlug: String, principal: Principal): RoomStatsDTO {
		val room = ensureRoom(roomSlug)
		val user = userService.getUser(principal.name)

		if (!isUserInRoom(room, user)) {
			throw NotInRoomException(room.slug, user.username)
		}

		return actionsService.getRoomStats(room)
	}

	@PostMapping("{roomSlug}/join")
	fun join(@PathVariable roomSlug: String, principal: Principal) = RoomDTO.create(
		roomService.joinRoom(
			joiner = userService.getUser(principal.name),
			room = ensureRoom(roomSlug)
		)
	)

	@PostMapping("{roomSlug}/start")
	fun start(@PathVariable roomSlug: String, principal: Principal) = RoomDTO.create(
		roomService.startRoom(
			starter = userService.getUser(principal.name),
			room = ensureRoom(roomSlug)
		)
	)

	@GetMapping("{roomSlug}/recommend")
	fun recommend(@PathVariable roomSlug: String, principal: Principal) = MovieDTO.create(
		actionsService.recommend(
			user = userService.getUser(principal.name),
			room = ensureRoom(roomSlug)
		)
	)

	@PostMapping("{roomSlug}/like")
	fun like(@PathVariable roomSlug: String, @RequestBody body: MovieIdDTO, principal: Principal): Boolean {
		actionsService.likeMovie(
			user = userService.getUser(principal.name),
			room = ensureRoom(roomSlug),
			movie = ensureMovie(body.movieId)
		)

		return true
	}

	@PostMapping("{roomSlug}/drop")
	fun drop(@PathVariable roomSlug: String, principal: Principal): Boolean {
		roomService.dropRoom(
			actor = userService.getUser(principal.name),
			room = ensureRoom(roomSlug)
		)

		return true
	}

	private fun isUserInRoom(room: Room, user: User) =
		room.users.find { it.id == user.id } != null

	private fun ensureRoom(slug: String) =
		roomService.getRoomBySlug(slug) ?: throw NoRoomException(slug)

	private fun ensureMovie(id: String) =
		movieService.getMovieById(id) ?: throw NoMovieException(id)
}
