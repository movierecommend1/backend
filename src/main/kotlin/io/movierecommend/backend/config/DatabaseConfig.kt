package io.movierecommend.backend.config

import com.mongodb.client.MongoClients
import io.movierecommend.backend.config.properties.MongoProperties
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@Configuration
@EnableMongoRepositories("io.movierecommend.backend.repositories")
class DatabaseConfig(
	val mongoConfig: MongoProperties
) : AbstractMongoClientConfiguration() {
	override fun mongoClient() = MongoClients.create(mongoConfig.connectionString)

	override fun getDatabaseName() = mongoConfig.database
}
