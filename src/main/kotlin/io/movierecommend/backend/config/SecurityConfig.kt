package io.movierecommend.backend.config

import io.movierecommend.backend.auth.UnauthorizedEntryPoint
import io.movierecommend.backend.config.properties.SecurityProperties
import io.movierecommend.backend.filters.BearerAuthFilter
import io.movierecommend.backend.services.auth.AuthService
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.SecurityScheme
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@SecurityScheme(
	name = "moviebackend-bearer",
	type = SecuritySchemeType.HTTP,
	bearerFormat = "JWT",
	scheme = "bearer",
	description = "A token can be acquired by performing /user/login request"
)
class SecurityConfig(
	private val userDetailsService: UserDetailsService,
	private val authService: AuthService,
	private val passwordEncoder: PasswordEncoder,
	private val securityProperties: SecurityProperties
) : WebSecurityConfigurerAdapter() {
	override fun configure(@Autowired auth: AuthenticationManagerBuilder?) {
		(auth ?: return)
			.userDetailsService(userDetailsService)
			.passwordEncoder(passwordEncoder)
	}

	fun corsConfig(): CorsConfigurationSource =
		if (securityProperties.cors == null) {
			CorsConfigurationSource {
				CorsConfiguration()
					.applyPermitDefaultValues()
					.also { it.addAllowedMethod(HttpMethod.OPTIONS) }
			}
		} else {
			CorsConfigurationSource {
				CorsConfiguration()
					.also { it.allowedOriginPatterns = securityProperties.cors.origins }
					.also { it.allowedHeaders = securityProperties.cors.headers }
					.also { it.allowedMethods = securityProperties.cors.methods }
			}
		}

	override fun configure(http: HttpSecurity?) {
		var config = (http ?: return)
			.csrf().disable()
			.cors().configurationSource(corsConfig())
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
			.antMatchers("/user/*").permitAll()
			.antMatchers("/ws-api").permitAll()

		if (securityProperties.swaggerAllowed) {
			config = config.antMatchers(
				"/v3/api-docs",
				"/configuration/ui",
				"/swagger-resources/**",
				"/configuration/security",
				"/swagger-ui/**",
				"/swagger-ui.html",
				"/webjars/**"
			).permitAll()
		}

		config
			.anyRequest().authenticated()
			.and()
			.exceptionHandling()
			.authenticationEntryPoint(UnauthorizedEntryPoint())
			.and()
			.addFilterBefore(
				BearerAuthFilter(authService),
				UsernamePasswordAuthenticationFilter::class.java
			)
	}
}
