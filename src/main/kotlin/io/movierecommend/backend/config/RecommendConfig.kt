package io.movierecommend.backend.config

import com.fasterxml.jackson.databind.ObjectMapper
import io.movierecommend.backend.config.properties.RecommendProperties
import io.movierecommend.backend.services.MovieService
import io.movierecommend.backend.services.UserMovieAppearanceService
import io.movierecommend.backend.services.recommend.DummyMovieRecommendService
import io.movierecommend.backend.services.recommend.MicroMovieRecommendService
import io.movierecommend.backend.services.recommend.MovieRecommendService
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RecommendConfig(
	private val properties: RecommendProperties
) {
	@Bean
	fun service(
		movieService: MovieService,
		appearanceService: UserMovieAppearanceService,
		restTemplateBuilder: RestTemplateBuilder,
		objectMapper: ObjectMapper
	): MovieRecommendService =
		when (properties.impl) {
			"dummy" -> DummyMovieRecommendService(movieService)
			"micro" -> MicroMovieRecommendService(
				properties, restTemplateBuilder, objectMapper, movieService
			)
			else -> throw IllegalArgumentException("no such implementation!")
		}
}
