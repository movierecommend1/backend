package io.movierecommend.backend.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "mongo")
data class MongoProperties(
	val connectionString: String,
	val database: String
)
