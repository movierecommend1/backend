package io.movierecommend.backend.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "recommend")
data class RecommendProperties(
	val baseUrl: String,
	val impl: String
)
