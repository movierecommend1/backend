package io.movierecommend.backend.config.properties

import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.util.*

@ConstructorBinding
@ConfigurationProperties(prefix = "security")
data class SecurityProperties(
	private val jwtSecret: String,
	val tokenExpiry: Long,
	private val allowSwagger: String,
	val cors: CORS?
) {
	val swaggerAllowed: Boolean
		get() = allowSwagger.lowercase().trim() == "true"

	data class CORS(
		@Value("#{'\${security.cors.methods}'.split(',')}")
		val origins: List<String>,
		@Value("#{'\${security.cors.methods}'.split(',')}")
		val methods: List<String>,
		@Value("#{'\${security.cors.headers}'.split(',')}")
		val headers: List<String>
	)

	val signingKey
		get() =
			Keys.hmacShaKeyFor(
				Base64.getDecoder()
					.decode(jwtSecret)
			)
}
