package io.movierecommend.backend.repositories

import io.movierecommend.backend.documents.Room
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface RoomRepository : MongoRepository<Room, ObjectId> {
	@Query("{ users: { \$elemMatch: { id: ?0 } } }")
	fun findAllByUserId(userId: ObjectId): List<Room>

	fun findBySlug(slug: String): Room?

	fun existsBySlug(slug: String): Boolean
}
