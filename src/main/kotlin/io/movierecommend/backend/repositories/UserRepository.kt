package io.movierecommend.backend.repositories

import io.movierecommend.backend.documents.User
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : MongoRepository<User, ObjectId> {
	fun findByUsername(username: String): User?

	fun existsByUsername(username: String): Boolean
}
