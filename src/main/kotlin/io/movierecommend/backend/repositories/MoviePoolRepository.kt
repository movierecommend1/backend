package io.movierecommend.backend.repositories

import io.movierecommend.backend.documents.MoviePoolEntry
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface MoviePoolRepository : MongoRepository<MoviePoolEntry, ObjectId> {
	@Query("{ 'room.\$id': ObjectId(?0) }")
	fun findAllByRoomId(roomId: ObjectId): List<MoviePoolEntry>

	@Query("{ 'room.\$id': ObjectId(?0) }", delete = true)
	fun deleteAllByRoomId(roomId: ObjectId)
}
