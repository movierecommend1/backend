package io.movierecommend.backend.repositories

import io.movierecommend.backend.documents.Movie
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.stream.Stream

interface MovieRepository : MongoRepository<Movie, ObjectId> {
	fun findAllByIdNotIn(ids: List<ObjectId>): Stream<Movie>
}
