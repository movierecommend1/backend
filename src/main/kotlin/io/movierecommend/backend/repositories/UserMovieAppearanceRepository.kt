package io.movierecommend.backend.repositories

import io.movierecommend.backend.documents.UserMovieAppearance
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface UserMovieAppearanceRepository : MongoRepository<UserMovieAppearance, ObjectId> {
	@Query("{ 'user.\$id': ObjectId(?0), 'movie.\$id': ObjectId(?1), 'room.\$id': ObjectId(?2) }")
	fun find(userId: ObjectId, movieId: ObjectId, roomId: ObjectId): UserMovieAppearance?

	@Query("{ 'room.\$id': ObjectId(?0) }")
	fun findAllByRoomId(roomId: ObjectId): List<UserMovieAppearance>

	@Query("{ 'room.\$id': ObjectId(?0), 'movie.\$id': ObjectId(?1) }")
	fun findAllByRoomAndMovieId(roomId: ObjectId, movieId: ObjectId): List<UserMovieAppearance>

	@Query("{ 'room.\$id': ObjectId(?0), 'user.\$id': ObjectId(?1), 'liked': true }")
	fun findLikedByRoomAndUserId(roomId: ObjectId, userId: ObjectId): List<UserMovieAppearance>

	@Query("{ 'room.\$id': ObjectId(?0), 'user.\$id': ObjectId(?1) }")
	fun findAllByRoomAndUserId(roomId: ObjectId, userId: ObjectId): List<UserMovieAppearance>

	@Query("{ 'room.\$id': ObjectId(?0) }", delete = true)
	fun deleteAllByRoomId(roomId: ObjectId)
}
